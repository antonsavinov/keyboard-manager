package com.example.keyboardmanager;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Class that describes dialogue that is located at the bottom of the screen with semi transparent background.
 */
public abstract class BottomDialogue extends BottomSheetDialog {

    /**
     * Interface that describes click listener
     */
    public interface OnClick {
        /**
         * This method will be performed when user taps by dialogue button
         *
         * @param buttonId The id of tapped button
         */
        void onButtonClicked(int buttonId);
    }

    static final int CONFIRMATION_TYPE = 100;
    static final int INFO_TYPE = 101;

    @IntDef({CONFIRMATION_TYPE, INFO_TYPE})
    @Retention(RetentionPolicy.SOURCE)
    @interface DialogueType {
    }

    /**
     * @return The type of dialogue. For now it can be or CONFIRMATION_TYPE either INFO_TYPE
     */
    abstract @DialogueType
    int getType();

    private OnClick mOnClick;

    private TextView mPositiveButtonTextView;
    private TextView mNegativeButtonTextView;

    private TextView mDialogueTextView;

    private Context mContext;

    private View.OnClickListener mOnViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mOnClick != null) {
                mOnClick.onButtonClicked(v.getId());
            }
        }
    };

    /**
     * Creates BottomDialogue with semi transparent background
     *
     * @param context The context
     */
    BottomDialogue(@NonNull Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        View view = getLayoutInflater().inflate(R.layout.custom_dialog_layout, null);
        setContentView(view);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        FrameLayout bottomSheet = findViewById(R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View view, int i) {
                    if (i == BottomSheetBehavior.STATE_DRAGGING) {
                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }

                @Override
                public void onSlide(@NonNull View view, float v) {

                }
            });
        }

        mDialogueTextView = view.findViewById(R.id.dialog_message);

        mPositiveButtonTextView = view.findViewById(R.id.positive_action);
        mNegativeButtonTextView = view.findViewById(R.id.negative_action);

        mPositiveButtonTextView.setOnClickListener(mOnViewClickListener);
        mNegativeButtonTextView.setOnClickListener(mOnViewClickListener);
    }

    /**
     * Sets the text at the bottom part of the dialogue.
     *
     * @param text The text
     */
    public void setText(String text) {
        mDialogueTextView.setText(text);
    }

    /**
     * Sets the click listener for dialogue.
     *
     * @param listener The listener
     */
    public void setOnClickListener(OnClick listener) {
        mOnClick = listener;
    }

    /**
     * Sets the positive button for Dialogue. @see DialogueButton class.
     *
     * @param positiveButton The positive button.
     */
    public void setPositiveButton(DialogueButton positiveButton) {
        setDialogueButton(positiveButton, true);
    }

    void setDialogueButton(DialogueButton button, boolean isPositiveButton) {
        TextView textView = isPositiveButton ? mPositiveButtonTextView : mNegativeButtonTextView;
        if (button.getIconId() == -1) {
            textView.setGravity(Gravity.CENTER);
            textView.setCompoundDrawablePadding(0);
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        }
        textView.setText(button.getText());
        textView.setId(button.getId());
    }
}
