package com.example.keyboardmanager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.appcompat.widget.AppCompatButton;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private GestureDetector mDetector;
    private AppCompatButton mButton;
    private KeyboardManager mKeyboardManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton = findViewById(R.id.customButton);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogue();
            }
        });

        mDetector = new GestureDetector(this, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                hideNavigationBar();
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                hideNavigationBar();
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                hideNavigationBar();
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });

        getWindow().getDecorView().setOnTouchListener(mTouchListener);

        mKeyboardManager = new KeyboardManager(this);

        final EditText myEditText = findViewById(R.id.edittext);
        findViewById(R.id.constraintlayout).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (myEditText.isFocused()) {
                        Rect outRect = new Rect();
                        myEditText.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            myEditText.clearFocus();
                            InputMethodManager imm =
                                    (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideNavigationBar();
        mKeyboardManager.registerKeyboardListener(new KeyboardManager.OnKeyboardListener() {
            @Override
            public void onKeyboardVisible() {
                Log.d(TAG, "onKeyboardVisible");
            }

            @Override
            public void onKeyboardHidden() {
                Log.d(TAG, "onKeyboardHidden");
            }
        }, findViewById(android.R.id.content));
    }

    @Override
    protected void onPause() {
        super.onPause();
        mKeyboardManager.unregisterKeyboardListener();
    }

    void showDialogue() {
        final ConfirmationDialogue dialogue = new ConfirmationDialogue(this);
        dialogue.setText("Some dialog message");

        DialogueButton positiveButton = new DialogueButton(BUTTON_POSITIVE, "Apply");
        dialogue.setPositiveButton(positiveButton);

        DialogueButton negativeButton =
                new DialogueButton(BUTTON_NEGATIVE, "Cancel");
        dialogue.setNegativeButton(negativeButton);

        dialogue.setOnClickListener(new BottomDialogue.OnClick() {
            @Override
            public void onButtonClicked(int buttonId) {
                switch (buttonId) {
                    case BUTTON_POSITIVE:
                        break;
                    case BUTTON_NEGATIVE:
                        break;
                }
                hideNavigationBar();
                dialogue.dismiss();
            }
        });
        dialogue.show();
    }

    View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return mDetector.onTouchEvent(event);
        }
    };

    private void hideNavigationBar() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
