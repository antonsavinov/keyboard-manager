package com.example.keyboardmanager;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

/**
 * Class that describes confirmation dialogue. Confirmation dialogue has two buttons: Positive and negative.
 */
final public class ConfirmationDialogue extends BottomDialogue {

    public static int NORMAL_TYPE = 100;
    public static int WARNING_TYPE = 101;

    @Override
    int getType() {
        return CONFIRMATION_TYPE;
    }

    /**
     * Constructs Confirmation dialogue with given parameters
     *
     * @param context The context
     */
    public ConfirmationDialogue(@NonNull Context context) {
        super(context);
    }

    /**
     * Sets negative button for Confirmation dialogue
     *
     * @param button The dialogue button. @see DialogueButton class.
     */
    public void setNegativeButton(DialogueButton button) {
        setDialogueButton(button, false);
    }
}
