package com.example.keyboardmanager;

/**
 * Class that describes button of dialogue.
 */
public class DialogueButton {
    private int mId;
    private int mIconId = -1;
    private String mText;

    /**
     * Constructs dialogue button with given parameters.
     *
     * @param id     The identification of button.
     * @param text   The title of button.
     * @param iconId The id of button icon.
     */
    public DialogueButton(int id, String text, int iconId) {
        this(id, text);
        mIconId = iconId;
    }

    /**
     * Constructs dialogue button without icon with given parameters.
     *
     * @param id   The identification of button.
     * @param text The title of button.
     */
    public DialogueButton(int id, String text) {
        mId = id;
        mText = text;
    }

    /**
     * @return The id of button
     */
    public int getId() {
        return mId;
    }

    /**
     * @return The title of button
     */
    public String getText() {
        return mText;
    }

    /**
     * @return The id of icon
     */
    public int getIconId() {
        return mIconId;
    }
}
